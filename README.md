# ABRomics workflow

**Content**

- [Genomic](#genome)
    - [Paired-end illumina reads](#paired-end-illumina-reads)
        - [Quality and Contamination control workflow for paired end data](#quality-and-Contamination-control-workflow-for-paired-end-data)
        - [Bacterial genome assembly workflow for paired end data](#bacterial-genome-assembly-workflow-for-paired-end-data)
        - [Bacterial genome annotation workflow](#bacterial-genome-annotation-workflow)
        - [AMR gene detection workflow in an assembled bacterial genome](#amr-gene-detection-workflow-in-an-assembled-bacterial-genome)
    - [Hybrid long reads](#hybrid-long-reads)
- [Metagenomic](#metogenoic)


## Genomic

### Paired-end illumina reads

#### Quality and Contamination control workflow for paired end data

This workflow uses paired-end illumina fastq(.gz) files and executes the following steps:
1. Quality control and trimming
    - **fastp** QC control and trimming
2. Taxonomic assignation on trimmed data
    - **Kraken2** assignation
    - **Bracken** to re-estimate abundance to the species level
    - **Recentrifuge** to make a krona chart
3. Aggregating outputs into a single JSON file
    - **ToolDistillator** to extract and aggregate information from different tool outputs to JSON parsable files

**Repository IWC:** [workflows/genome-assembly/quality-and-contamination-control](https://github.com/galaxyproject/iwc/tree/main/workflows/genome-assembly/quality-and-contamination-control)


#### Bacterial genome assembly workflow for paired end data

This workflow uses paired-end illumina trimmed reads fastq(.gz) files and executes the following steps:
1. Assembly raw reads to a final contig fasta file 
    - **Shovill**
2. Quality control of the assembly
    - **Quast**
    - **Bandage** to plot assembly graph
    - **Refseqmasher** to identify the closed reference genome
3. Aggregating outputs into a single JSON file
    - **ToolDistillator** to extract and aggregate information from different tool outputs to JSON parsable files

**Repository IWC:** [workflows/genome-assembly/bacterial-genome-assembly](https://github.com/galaxyproject/iwc/tree/main/workflows/genome-assembly/bacterial-genome-assembly)


#### Bacterial genome annotation workflow

This workflow uses assembled bacterial genome fasta files (but can be any fasta file) and executes the following steps:
1. Genomic annotation
    - **Bakta** to predict CDS and small proteins (sORF)
2. Integron identification
    - **IntegronFinder2** to identify CALIN elements, In0 elements, and complete integrons
3. Plasmid gene identification
    - **Plasmidfinder** to identify and typing plasmid sequences
4. Inserted sequence (IS) detection
    - **ISEScan** to detect IS elements
5. Aggregating outputs into a single JSON file
    - **ToolDistillator** to extract and aggregate information from different tool outputs to JSON parsable files

**Repository IWC:** [workflows/bacterial_genomics/bacterial_genome_annotation](https://github.com/galaxyproject/iwc/tree/main/workflows/bacterial_genomics/bacterial_genome_annotation)


#### AMR gene detection workflow in an assembled bacterial genome

This workflow uses assembled bacterial genome fasta files (but can be any fasta file) and executes the following steps:
1. Genomic detection
    - Antimicrobial resistance gene identification:
        - **staramr** to blast against ResFinder and PlasmidFinder database
        - **AMRFinderPlus** to find antimicrobial resistance genes and point mutations 
    - Virulence gene identification:
        - **ABRicate** with VFDB_A database
2. Aggregating outputs into a single JSON file
    - **ToolDistillator** to extract and aggregate information from different tool outputs to JSON parsable files

**Repository IWC:** [workflows/bacterial_genomics/amr_gene_detection](https://github.com/galaxyproject/iwc/tree/main/workflows/bacterial_genomics/amr_gene_detection)


### Hybrid long reads


## Metagenomic